
/* Auto Magically Generated file */
/* Do not edit! */
#pragma once

#define PX4_GIT_VERSION_STR "26ea70e729c0f5fed280aac3bac9cb066079bfd2"
#define PX4_GIT_VERSION_BINARY 0x26ea70e729c0f5fe
#define PX4_GIT_TAG_STR "v1.13.0-alpha1-4285-g26ea70e729-dirty"
#define PX4_GIT_BRANCH_NAME ""

#define PX4_GIT_OEM_VERSION_STR  ""

#define PX4_GIT_TAG_OR_BRANCH_NAME "master" // special variable: git tag, release or master branch

#define MAVLINK_LIB_GIT_VERSION_STR  "4b0558d0d10efbdd550cb5321d56f6a611d0ab14"
#define MAVLINK_LIB_GIT_VERSION_BINARY 0x4b0558d0d10efbdd

#define NUTTX_GIT_VERSION_STR  "017aa15746bcf6b57bb185e14dce7fa0876fc751"
#define NUTTX_GIT_VERSION_BINARY 0x017aa15746bcf6b5
#define NUTTX_GIT_TAG_STR  "v8.2.0"
