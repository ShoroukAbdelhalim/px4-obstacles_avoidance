#include <px4_platform_common/px4_config.h>
#include <px4_platform_common/log.h>
#include <uORB/Publication.hpp>
#include <uORB/topics/led_control.h>
#include <uORB/topics/debug_value.h>

#include <drivers/drv_hrt.h>
#include <uORB/topics/test_motor.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/input_rc.h>

#define DC_MOTOR 0
#define SR_MOTOR 1
#define MAX_SPEED 0.56
#define BACK_MAX_SPEED (1-MAX_SPEED)
#define CAM_LEFT 0
#define CAM_RIGHT 2
#define CAM_FORWARD 1

extern "C" __EXPORT int hello_world_main(int argc, char *argv[]);

int hello_world_main(int argc, char *argv[])
{
	px4_sleep(2);
	debug_value_s debug_data;
	sensor_combined_s sensor_data;
	input_rc_s sensor_rc;
	

	int debug_handle = orb_subscribe(ORB_ID(debug_value));
	int sensor_combined_handle = orb_subscribe(ORB_ID(sensor_combined));
	int input_rc_handle = orb_subscribe(ORB_ID(input_rc));
	
	orb_set_interval(debug_handle, 100);
	orb_set_interval(sensor_combined_handle, 200);
	orb_set_interval(input_rc_handle, 200);
	
	led_control_s led_control;
	test_motor_s test_motor;
	test_motor_s test_servo;
		
	uORB::Publication<led_control_s> led_control_pub(ORB_ID(led_control));
	uORB::Publication<test_motor_s> test_motor_pub(ORB_ID(test_motor));

	test_motor.timestamp = hrt_absolute_time();
	test_motor.motor_number = DC_MOTOR;
	test_motor.action = test_motor_s::ACTION_RUN;
	test_motor.driver_instance = 0;
	test_motor.timeout_ms = 0;	
	test_motor.value = (float)0.5;
	test_motor_pub.publish(test_motor);
	
	
	while(1)
	{
		orb_copy(ORB_ID(debug_value), debug_handle, &debug_data);
		orb_copy(ORB_ID(sensor_combined), sensor_combined_handle, &sensor_data);
		orb_copy(ORB_ID(input_rc), input_rc_handle, &sensor_rc);
		
		test_motor.timestamp = hrt_absolute_time();
		test_motor.motor_number = DC_MOTOR;
		test_motor.action = test_motor_s::ACTION_RUN;
		test_motor.driver_instance = 0;
		test_motor.timeout_ms = 0;
		
		test_servo.timestamp = hrt_absolute_time();
		test_servo.motor_number = SR_MOTOR;
		test_servo.action = test_motor_s::ACTION_RUN;
		test_servo.driver_instance = 0;
		test_servo.timeout_ms = 0;
		
		led_control.timestamp = hrt_absolute_time();
		led_control.mode = led_control_s::MODE_ON;
		led_control.priority = led_control_s::MAX_PRIORITY;
		led_control.led_mask = 0xff;
		
		
		
		// off state
		if ((double)sensor_rc.values[4] <1500)
		{
			test_motor.value = (float)0.5;
			test_servo.value = (float)0.5;
			led_control.color = led_control_s::COLOR_WHITE;
		}
		
		// RC controlled state
		else if ((double)sensor_rc.values[4] < 2000)
		{
			// speed adjustment
			if ((double)sensor_rc.values[2] > 1600)
			{
				test_motor.value = (float)0.5 + (sensor_rc.values[2] - 1600) *(float) (MAX_SPEED-0.5)/(1960-1600);
				led_control.color = led_control_s::COLOR_GREEN;
			}
			else if ((double)sensor_rc.values[2] < 1500)
			{
				test_motor.value = (float)0.5 - (1500 - sensor_rc.values[2]) *(float) (0.5 - BACK_MAX_SPEED)/(1500-1024);
				led_control.color = led_control_s::COLOR_BLUE;
			}
			else
			{
				test_motor.value = (float)0.5;
				led_control.color = led_control_s::COLOR_RED;
			}
			
			// direction adjustment
			if ((double)sensor_rc.values[0] > 1500)
			{
				test_servo.value = (float)0.5 - (sensor_rc.values[0] - 1450) *(float) (0.5)/(1450-1024);
			}
			else if ((double)sensor_rc.values[0] < 1450)
			{
				test_servo.value = (float)0.5 + (1450 - sensor_rc.values[0]) *(float) (1-0.5)/(1450 - 1030);
			}
			else
			{
				test_servo.value = (float)0.5;
			}
		
		}
		
		
		
		// Autonomous state
		else
		{
			if (debug_data.value <20 )
			{
				test_motor.value = (float)0.5;
				led_control.color = led_control_s::COLOR_RED;
			}
			else if(debug_data.value <60)
			{
				test_motor.value = (float)0.5 + (debug_data.value - 5) *(float) (MAX_SPEED-0.5)/(40-5);
				led_control.color = led_control_s::COLOR_YELLOW;
			}
			else 
			{
				test_motor.value = (float)MAX_SPEED;
				led_control.color = led_control_s::COLOR_GREEN;
			}
			
			if (debug_data.ind == CAM_LEFT)
			{
				test_servo.value = 1;
			}
			else if (debug_data.ind == CAM_RIGHT)
			{
				test_servo.value = 0;
			}
			else 
			{
				test_servo.value = (float)0.5;
			}
		
			//if ((double)sensor_data.accelerometer_m_s2[0] >4)
			{
			//	temp = test_motor.value*0.08;
			//	test_motor.value = (float)test_motor.value - (float)(temp);	
			//	if (test_motor.value < 0.5)
			//		test_motor.value = (float)0.5;
			}
			//else if((double)sensor_data.accelerometer_m_s2[0] > 2)
			{
			//	test_motor.value = (float)test_motor.value - (float)(test_motor.value*0.04);	
			//	if (test_motor.value < 0.5)
			//		test_motor.value = (float)0.5;
			}
			//else if((double)sensor_data.accelerometer_m_s2[0] > -2)
			{
			//	test_motor.value = (float)test_motor.value;
			}
			//else if((double)sensor_data.accelerometer_m_s2[0] > -4)
			{
			//	test_motor.value = (float)test_motor.value + (float)(test_motor.value*0.05);
			}
			//else
			{
			//	test_motor.value = (float)test_motor.value + (float)(test_motor.value*0.1);
			}
		
		}

		
		test_motor_pub.publish(test_motor);
		led_control_pub.publish(led_control);
		test_motor_pub.publish(test_servo);
		
		
		// terminate the program
		if((double)sensor_rc.values[5] < 1500)
			break;
		
		
		px4_usleep(200000);
	}
	
		test_motor.timestamp = hrt_absolute_time();
		test_motor.motor_number = DC_MOTOR;
		test_motor.value = (float)0.5;
		test_motor.driver_instance = 0;
		test_motor.timeout_ms = 0;
		test_motor_pub.publish(test_motor);
				
		return 0;
}
		
